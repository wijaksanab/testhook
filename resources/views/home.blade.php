@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    Halo {{ Auth::user()->name }} </br>
                    NIM anda {{ Auth::user()->nim }} </br>
                    <hr>

                    <h4> Selamat datang {{ Auth::user()->username }}, Anda Sudah Login Di Auth Laravel Login</h4>
                    Disini belum ada apa apa ngapain juga kesini , ingin logout? yuk <a href="{{ route('logout') }}"
                                  onclick="event.preventDefault();
                                  document.getElementById('logout-form').submit();">
                     {{ __('Logout !') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
